#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: Carton\n"
"POT-Creation-Date: 2016-03-01 10:44-0500\n"
"PO-Revision-Date: 2016-03-01 10:44-0500\n"
"Last-Translator: c.bavota <cbavota@gmail.com>\n"
"Language-Team: c.bavota <cbavota@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7\n"
"X-Poedit-Basepath: ../..\n"
"X-Poedit-WPHeader: style.css\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: 404.php:8
msgid "404 Error"
msgstr ""

#: 404.php:11
msgid "Sorry. We can't seem to find the page you are looking for."
msgstr ""

#: comments.php:3
msgid ""
"This post is password protected. Enter the password to view any comments."
msgstr ""

#: comments.php:19
#, php-format
msgid "1 comment for &ldquo;%2$s&rdquo;"
msgid_plural "%1$s comments for &ldquo;%2$s&rdquo;"
msgstr[0] ""
msgstr[1] ""

#: comments.php:26 comments.php:38
msgid "Comment navigation"
msgstr ""

#: comments.php:27 comments.php:39
msgid "&larr; Older Comments"
msgstr ""

#: comments.php:28 comments.php:40
msgid "Newer Comments &rarr;"
msgstr ""

#: comments.php:50
msgid "Comments are closed."
msgstr ""

#: content-aside.php:9
msgid "Aside"
msgstr ""

#: content-aside.php:12 content-image.php:16 content-link.php:12
#: content-quote.php:11 content-status.php:20 content.php:19 functions.php:265
msgid "Read more &rarr;"
msgstr ""

#: content-footer.php:11
msgid "Pages:"
msgstr ""

#: content-footer.php:12 functions.php:203 functions.php:225
msgid "(edit)"
msgstr ""

#: content-footer.php:13
msgid "Tags:"
msgstr ""

#: content-gallery.php:35
#, php-format
msgid "This gallery contains <a %1$s>%2$s photo &rarr;</a>"
msgid_plural "This gallery contains <a %1$s>%2$s photos &rarr;</a>"
msgstr[0] ""
msgstr[1] ""

#: content-gallery.php:35
#, php-format
msgid "Permalink to %s"
msgstr ""

#: content-header.php:27
#, php-format
msgid "by %s"
msgstr ""

#: content-header.php:28
#, php-format
msgid "Posts by %s"
msgstr ""

#: content-header.php:44
msgid "0 Comments"
msgstr ""

#: content-header.php:44
msgid "1 Comment"
msgstr ""

#: content-header.php:44
msgid "% Comments"
msgstr ""

#: content-link.php:9
msgid "Link"
msgstr ""

#: content-none.php:9 search.php:35
msgid "Nothing found"
msgstr ""

#: content-none.php:12
msgid "No results were found. Please try again."
msgstr ""

#: content-status.php:10
msgid "Status"
msgstr ""

#: content-status.php:17
#, php-format
msgid "Posted on %1$s at %2$s"
msgstr ""

#: footer.php:12
#, php-format
msgid "Copyright &copy; %s %s. All Rights Reserved."
msgstr ""

#: footer.php:13
#, php-format
msgid "The %s Theme by %s."
msgstr ""

#: functions.php:58
msgid "Primary Menu"
msgstr ""

#: functions.php:131
msgid "No more posts."
msgstr ""

#: functions.php:157
msgid "First Sidebar"
msgstr ""

#: functions.php:159
msgid ""
"This is the sidebar widgetized area. All defaults widgets work great here."
msgstr ""

#: functions.php:202
#, php-format
msgid "%1$s at %2$s"
msgstr ""

#: functions.php:207
msgid "Your comment is awaiting moderation."
msgstr ""

#: functions.php:225
msgid "Pingback:"
msgstr ""

#: functions.php:336 single.php:10
msgid "Post navigation"
msgstr ""

#: functions.php:337
msgid "&larr; Older posts"
msgstr ""

#: functions.php:337
msgid "Newer posts &rarr;"
msgstr ""

#: header.php:44
msgid "Main menu"
msgstr ""

#: header.php:45
msgid "Skip to content"
msgstr ""

#: library/about.php:26
#, php-format
msgid ""
"Thanks for choosing %s! You can learn how to use the available theme options "
"on the %sabout page%s."
msgstr ""

#: library/about.php:28
#, php-format
msgid "Learn more about %s"
msgstr ""

#: library/about.php:41 library/preview-pro.php:82
#, php-format
msgid "Welcome to %s %s"
msgstr ""

#: library/about.php:41
#, php-format
msgid "About %s"
msgstr ""

#: library/about.php:104
#, php-format
msgid ""
"Read through the following documentation to learn more about <em>%s</em> and "
"how to use the available theme options."
msgstr ""

#: library/about.php:111 library/preview-pro.php:92
msgid "Documentation"
msgstr ""

#: library/about.php:112 library/preview-pro.php:93
msgid "Upgrade"
msgstr ""

#: library/about.php:116
msgid "The Customizer"
msgstr ""

#: library/about.php:122
#, php-format
msgid ""
"All theme options for <em>%s</em> are controlled under %sAppearance &rarr; "
"Customize%s. From there, you can modify layout, custom menus, and more."
msgstr ""

#: library/about.php:124 library/customizer.php:54
msgid "Layout"
msgstr ""

#: library/about.php:125
msgid ""
"Carton allow you to set the width for the home page columns and the left "
"sidebar. Play around with the number until you get one that perfectly suits "
"your site."
msgstr ""

#: library/about.php:127 library/customizer.php:82
msgid "Posts"
msgstr ""

#: library/about.php:128
msgid ""
"You can display different elements in a post&rsquo;s byline by checking the "
"appropriate boxes. Choose between date, author, categories and comments."
msgstr ""

#: library/about.php:133
msgid "Custom Menus"
msgstr ""

#: library/about.php:135
#, php-format
msgid ""
"<em>%s</em> includes one Custom Menu locations: the Primary sidebar menu."
msgstr ""

#: library/about.php:137
#, php-format
msgid ""
"To add a navigation menu to the sidebar, go to %sAppearance &rarr; Menus%s. "
"By default, a list of categories will appear in this location if no custom "
"menu is set."
msgstr ""

#: library/about.php:139
msgid ""
"Clicking over a top-level link in the primary navigation will open up the "
"first dropdown list of sub-menu links."
msgstr ""

#: library/about.php:145
msgid "Jetpack"
msgstr ""

#: library/about.php:151
msgid "Tiled Galleries"
msgstr ""

#: library/about.php:153
#, php-format
msgid ""
"%sJetpack&rsquo;s Tiled Galleries%s will display your images in a beautiful "
"mosaic layout. Go to %sJetpack &rarr; Settings%s to turn it on. "
msgstr ""

#: library/about.php:155
msgid "Carousel"
msgstr ""

#: library/about.php:157
#, php-format
msgid ""
"With %sJetpack&rsquo;s Carousel%s, clicking on one of your gallery images "
"will open up a featured lightbox slideshow. Turn it on by going to %sJetpack "
"&rarr; Settings%s. "
msgstr ""

#: library/about.php:162
msgid "Pull Quotes"
msgstr ""

#: library/about.php:168
msgid ""
"You can easily include a pull quote within your text by using the following "
"code within the Text/HTML editor:"
msgstr ""

#: library/about.php:170
msgid ""
"&lt;blockquote class=\"pullquote\"&gt;This is a pull quote that will appear "
"within your text.&lt;/blockquote&gt;"
msgstr ""

#: library/about.php:171
msgid "You can also align it to the right with this code:"
msgstr ""

#: library/about.php:173
msgid ""
"&lt;blockquote class=\"pullquote alignright\"&gt;This is a pull quote that "
"will appear within your text.&lt;/blockquote&gt;"
msgstr ""

#: library/about.php:177
#, php-format
msgid ""
"For more information, check out the %sDocumentation &amp; FAQs%s section on "
"my site."
msgstr ""

#: library/customizer.php:64
msgid "Column Width (px)"
msgstr ""

#: library/customizer.php:75
msgid "Sidebar Width (px)"
msgstr ""

#: library/customizer.php:92
msgid "Display Categories"
msgstr ""

#: library/customizer.php:103
msgid "Display Author"
msgstr ""

#: library/customizer.php:114
msgid "Display Date"
msgstr ""

#: library/customizer.php:125
msgid "Display Comment Count"
msgstr ""

#: library/preview-pro.php:19
#, php-format
msgid "Upgrade to %s Pro"
msgstr ""

#: library/preview-pro.php:19
msgid "Upgrade to Pro"
msgstr ""

#: library/preview-pro.php:85
#, php-format
msgid ""
"Take your site to the next level with the full version of %s. Check out some "
"of the advanced features that&rsquo;ll give you more control over your "
"site&rsquo;s layout and design. <br />%s"
msgstr ""

#: library/preview-pro.php:85
msgid "View the demo video &rarr;"
msgstr ""

#: library/preview-pro.php:97
msgid "Advanced Color Picker"
msgstr ""

#: library/preview-pro.php:102
#, php-format
msgid ""
"Sometimes the default colors just aren&rsquo;t working for you. In %s you "
"can use the advanced color picker to make sure you get the exact colors you "
"want."
msgstr ""

#: library/preview-pro.php:103
msgid ""
"Easily select one of the eight preset colors or dive even deeper into your "
"customization by using a more specific hex code."
msgstr ""

#: library/preview-pro.php:108
msgid "Google Fonts"
msgstr ""

#: library/preview-pro.php:113
msgid ""
"Web-safe fonts are a thing of the past, so why not try to spice things up a "
"bit?"
msgstr ""

#: library/preview-pro.php:114
msgid ""
"Choose from some of Google Fonts&rsquo; most popular fonts to improve your "
"site&rsquo;s typeface readability and make things look even more amazing."
msgstr ""

#: library/preview-pro.php:119
msgid "Custom CSS Editor"
msgstr ""

#: library/preview-pro.php:124
msgid ""
"Sometimes the Theme Options don't let you control everything you want. "
"That's where the Custom CSS Editor comes into play."
msgstr ""

#: library/preview-pro.php:125
msgid ""
"Use CSS to style any element without having to worry about losing your "
"changes when you update. All your Custom CSS is safely stored in the "
"database."
msgstr ""

#: library/preview-pro.php:130
msgid "Even More Theme Options"
msgstr ""

#: library/preview-pro.php:134
msgid "Bootstrap Shortcodes"
msgstr ""

#: library/preview-pro.php:135
#, php-format
msgid ""
"Shortcodes are awesome and easy to use. That&rsquo;s why %s comes with a "
"bunch, like a slideshow carousel, alert boxes and more."
msgstr ""

#: library/preview-pro.php:138
msgid "Import/Export Tool"
msgstr ""

#: library/preview-pro.php:139
msgid ""
"Once you&rsquo;ve set up your site exactly how you want, you can easily "
"export the Theme Options and Custom CSS for safe keeping."
msgstr ""

#: library/preview-pro.php:145
#, php-format
msgid "Buy %s Now &rarr;"
msgstr ""

#: search.php:13
msgid " search results for"
msgstr ""

#: search.php:39
msgid "No results were found for your search. Please try again."
msgstr ""

#: sidebar.php:6
#, php-format
msgid "Add your own widgets by going to the %sWidgets admin page%s."
msgstr ""

#: sidebar.php:10
msgid "Meta"
msgstr ""

#: sidebar.php:19
msgid "Archives"
msgstr ""

#: single.php:12
msgid "&larr; Previous Image"
msgstr ""

#: single.php:13
msgid "Next Image &rarr;"
msgstr ""

#: single.php:15
msgid "&larr; %title"
msgstr ""

#: single.php:16
msgid "%title &rarr;"
msgstr ""

#. Theme Name of the plugin/theme
msgid "Carton"
msgstr ""

#. Theme URI of the plugin/theme
msgid "https://themes.bavotasan.com/themes/carton-pro-wordpress-theme/"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Create a truly unique design with Carton, a lightweight and fully responsive "
"HTML5 theme built using jQuery Masonry. Use the new Theme Options customizer "
"to add your own header image, custom background, column width and more. "
"Distinguish each post with one of the eight supported post formats: Video, "
"Image, Aside, Status, Audio, Quote, Link and Gallery. Install JetPack to "
"display each of your galleries through a tiled view and jQuery carousel. "
"Compatible with bbPress & BuddyPress. Created using SASS and Compass. Uses "
"Google Fonts for improved typeface readability and works perfectly in "
"desktop browsers, tablets and handheld devices. For a live demo go to http://"
"demos.bavotasan.com/carton/."
msgstr ""

#. Author of the plugin/theme
msgid "c.bavota"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://bavotasan.com"
msgstr ""
