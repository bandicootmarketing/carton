<?php
/**
 * Set up the default theme options
 *
 * @since 1.0.0
 */
function bavotasan_default_theme_options() {
	//delete_option( 'theme_mods_carton' );
	return array(
		'column_width' => '320',
		'sidebar_width' => '250',
		'display_author' => 'on',
		'display_date' => 'on',
		'display_comment_count' => 'on',
		'display_categories' => 'on',
	);
}

function bavotasan_theme_options() {
	$bavotasan_default_theme_options = bavotasan_default_theme_options();

	$return = array();
	foreach( $bavotasan_default_theme_options as $option => $value ) {
		$return[$option] = get_theme_mod( $option, $value );
	}

	return $return;
}

class Bavotasan_Customizer {
	public function __construct() {
		add_action( 'customize_register', array( $this, 'customize_register' ) );

		$mods = get_option( 'theme_mods_carton' );
		if ( empty( $mods ) ) {
			add_option( 'theme_mods_carton', get_option( 'carton_theme_options' ) );
		}
	}

	/**
	 * Adds theme options to the Customizer screen
	 *
	 * This function is attached to the 'customize_register' action hook.
	 *
	 * @param	class $wp_customize
	 *
	 * @since 1.0.0
	 */
	public function customize_register( $wp_customize ) {
		$bavotasan_default_theme_options = bavotasan_default_theme_options();

		// Layout section panel
		$wp_customize->add_section( 'carton_layout', array(
			'title' => __( 'Layout', 'carton' ),
			'priority' => 35,
		) );

		$wp_customize->add_setting( 'column_width', array(
			'default' => $bavotasan_default_theme_options['column_width'],
            'sanitize_callback' => 'absint',
		) );

		$wp_customize->add_control( 'column_width', array(
			'label' => __( 'Column Width (px)', 'carton' ),
			'section' => 'carton_layout',
			'type' => 'text',
		) );

		$wp_customize->add_setting( 'sidebar_width', array(
			'default' => $bavotasan_default_theme_options['sidebar_width'],
            'sanitize_callback' => 'absint',
		) );

		$wp_customize->add_control( 'sidebar_width', array(
			'label' => __( 'Sidebar Width (px)', 'carton' ),
			'section' => 'carton_layout',
			'type' => 'text',
		) );

		// Posts panel
		$wp_customize->add_section( 'bavotasan_posts', array(
			'title' => __( 'Posts', 'carton' ),
			'priority' => 40,
		) );

		$wp_customize->add_setting( 'display_categories', array(
			'default' => $bavotasan_default_theme_options['display_categories'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_categories', array(
			'label' => __( 'Display Categories', 'carton' ),
			'section' => 'bavotasan_posts',
			'type' => 'checkbox',
		) );

		$wp_customize->add_setting( 'display_author', array(
			'default' => $bavotasan_default_theme_options['display_author'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_author', array(
			'label' => __( 'Display Author', 'carton' ),
			'section' => 'bavotasan_posts',
			'type' => 'checkbox',
		) );

		$wp_customize->add_setting( 'display_date', array(
			'default' => $bavotasan_default_theme_options['display_date'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_date', array(
			'label' => __( 'Display Date', 'carton' ),
			'section' => 'bavotasan_posts',
			'type' => 'checkbox',
		) );

		$wp_customize->add_setting( 'display_comment_count', array(
			'default' => $bavotasan_default_theme_options['display_comment_count'],
            'sanitize_callback' => array( $this, 'sanitize_checkbox' ),
		) );

		$wp_customize->add_control( 'display_comment_count', array(
			'label' => __( 'Display Comment Count', 'carton' ),
			'section' => 'bavotasan_posts',
			'type' => 'checkbox',
		) );
	}

	/**
	 * Sanitize checkbox options
	 *
	 * @since 1.0.2
	 */
    public function sanitize_checkbox( $value ) {
        if ( 'on' != $value )
            $value = false;

        return $value;
    }
}
$bavotasan_customizer = new Bavotasan_Customizer;